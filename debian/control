Source: winregfs
Section: utils
Priority: optional
Maintainer: Debian Security Tools <team+pkg-security@tracker.debian.org>
Uploaders: Giovani Augusto Ferreira <giovani@debian.org>
Build-Depends: debhelper-compat (= 13), libfuse-dev, pkg-config
Standards-Version: 4.6.2
Rules-Requires-Root: binary-targets
Homepage: https://github.com/jbruchon/winregfs
Vcs-Git: https://salsa.debian.org/pkg-security-team/winregfs.git
Vcs-Browser: https://salsa.debian.org/pkg-security-team/winregfs

Package: winregfs
Architecture: any
Depends: ${misc:Depends}, ${shlibs:Depends}
Description: Windows registry FUSE filesystem
 Winregfs is a FUSE-based filesystem driver that enables accessing of Windows
 registry hive files as ordinary filesystems. Registry hive file editing can
 be performed with ordinary shell scripts and command-line tools once mounted.
 .
 fsck.winregfs scans a Windows registry hive file for problems that indicate
 the hive has been damaged by hardware or software issues, reading recursively
 the  key  and  value  data structures in the registry hive.
 .
 This package provides mount.winregfs and fsck.winregfs commands.
 Winregfs is useful for pentesters, ethical hackers and forensics experts.
